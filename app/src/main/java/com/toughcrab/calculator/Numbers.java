package com.toughcrab.calculator;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;
import android.widget.Button;
import android.widget.EditText;

public class Numbers extends Activity implements View.OnClickListener {

    EditText number;
    Button sendInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_numbers);

        number = (EditText)findViewById(R.id.editText);
        sendInfo = (Button)findViewById(R.id.button);
        sendInfo.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        String s = number.getText().toString();
        Intent intent = getIntent();
        String message = intent.getStringExtra("numbers");

        if(message.contentEquals("width")) {
            intent.putExtra("widthInfo", s);
            setResult(RESULT_OK, intent);
            finish();
        }
        if (message.contentEquals("height")) {
            intent.putExtra("heightInfo", s);
            setResult(RESULT_OK, intent);
            finish();

        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.numbers, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }



    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_numbers, container, false);
            return rootView;
        }
    }

}

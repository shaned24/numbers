package com.toughcrab.calculator;

import android.content.Context;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class InternalStorage extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_internal_storage);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.internal_storage, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public class PlaceholderFragment extends Fragment implements View.OnClickListener{

        Button saveFile;
        EditText filename, content;
        String FILENAME, CONTENT;
        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_internal_storage, container, false);

            filename = (EditText) rootView.findViewById(R.id.filename);
            content = (EditText) rootView.findViewById(R.id.content);
            saveFile = (Button) rootView.findViewById(R.id.saveFile);
            saveFile.setOnClickListener(this);

            return rootView;
        }

        @Override
        public void onClick(View v) {
//            Context context = getApplicationContext();


            FILENAME = filename.getText().toString();
            if(FILENAME.contentEquals("")) {
                FILENAME = "Untitled";
            }
            CONTENT = content.getText().toString();

            try {

                FileOutputStream fos = openFileOutput(FILENAME, Context.MODE_PRIVATE);
                fos.write(CONTENT.getBytes());
                fos.close();
                showToast("saved");

            } catch (FileNotFoundException e) {
                e.printStackTrace();
                showToast("notSaved");
            } catch (IOException e) {
                e.printStackTrace();
                showToast("notSaved");
            }
            finish();

        }

        public void showToast(String type) {
            CharSequence stringSaved = "Saved!";
            CharSequence stringNotSaved = "Not Saved!";
            int duration = Toast.LENGTH_SHORT;

            Toast saved = Toast.makeText(getApplicationContext(), stringSaved, duration);
            Toast notSaved = Toast.makeText(getApplicationContext(), stringNotSaved, duration);

            if (type.equals("saved")) {
                saved.show();

            } else if (type.equals("notSaved")) {
                notSaved.show();

            }

        }
    }

}

package com.toughcrab.calculator;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity implements View.OnClickListener {

    Button width, height, calc, create_post, goToMenu;
    TextView area;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        width = (Button) findViewById(R.id.button);
        height = (Button) findViewById(R.id.button2);
        calc = (Button) findViewById(R.id.button3);
        area = (TextView)findViewById(R.id.textView);

        create_post = (Button)findViewById(R.id.create_post);
        goToMenu = (Button)findViewById(R.id.goToMenu);

        width.setOnClickListener(this);
        height.setOnClickListener(this);
        calc.setOnClickListener(this);
        create_post.setOnClickListener(this);
        goToMenu.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(this, Numbers.class);

        switch (v.getId()){
            case R.id.button:
                //width
                intent.putExtra("numbers", "width");
                startActivityForResult(intent, 1);
                break;
            case R.id.button2:
                 //height
                intent.putExtra("numbers", "height");
                startActivityForResult(intent, 1);
                break;
            case R.id.button3:
                //calc
                int a = Integer.valueOf(width.getText().toString());
                int b = Integer.valueOf(height.getText().toString());
                area.setText(a*b + " sq ft");
                break;
            case R.id.create_post :
                Intent create_post_intent = new Intent(this, InternalStorage.class);
                startActivity(create_post_intent);
                break;
            case R.id.goToMenu :
                Intent goToMenu_intent = new Intent(this, Menu1.class);
                startActivity(goToMenu_intent);
            default:
                break;

        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(data.getExtras().containsKey("widthInfo")) {
            width.setText(data.getStringExtra("widthInfo"));
        }
        if(data.getExtras().containsKey("heightInfo")) {
            height.setText(data.getStringExtra("heightInfo"));
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, Settings.class);

            startActivity(intent);
//            return true;
        }
        return super.onOptionsItemSelected(item);
    }





}

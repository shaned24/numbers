package com.toughcrab.calculator;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

public class Reading extends Activity implements OnClickListener, AdapterView.OnItemSelectedListener {

    Spinner spinner;
    TextView title, entry;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reading);
        spinner = (Spinner)findViewById(R.id.spinner1);
        title = (TextView)findViewById(R.id.textView1);
        entry = (TextView)findViewById(R.id.textView2);
        getFilenames();
    }

    private void getFilenames() {
        String[] filenames = getApplicationContext().fileList();
        List<String> list = new ArrayList<String>();
        for(int i = 0; i<filenames.length; i++)
        {
            Log.d("Filename QWEERTY", filenames[i]);
            list.add(filenames[i]);

        }
        ArrayAdapter<String> filenameAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, list);
        spinner.setAdapter(filenameAdapter);

        spinner.setOnItemSelectedListener(this);

    }


    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        String selectFile = String.valueOf(spinner.getSelectedItem());
        openFile(selectFile);
    }

    private void openFile(String selectFile) {

        String value = "";
        FileInputStream fis;

        try {
            fis = openFileInput(selectFile);
            byte[] input = new byte[fis.available()];
            while(fis.read(input) != -1) {
                value += new String(input);
            }
            fis.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        entry.setText(value);

    }



    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String selection =spinner.getSelectedItem().toString();
        openFile(selection);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}